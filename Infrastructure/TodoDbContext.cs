﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TodoApp_API.Models;

namespace TodoApp_API.Infrastructure
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext(DbContextOptions<TodoDbContext> options)
            : base(options)
        { }

        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<TodoTask> TodoTasks { get; set; }
    }
}
