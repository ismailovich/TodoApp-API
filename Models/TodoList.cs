﻿namespace TodoApp_API.Models
{
    public class TodoList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<TodoTask> Tasks { get; set; }
    }
}
