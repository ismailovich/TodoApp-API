﻿namespace TodoApp_API.Models
{
    public class TodoTask
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }
        public int TodoListId { get; set; }
        public TodoList TodoList { get; set; }
    }
}
